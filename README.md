# SP Github Server

## Basic Commands

### Docker Commands

#### Build the containers

```sh
$ docker-compose build
```

#### Run all containers
```sh
$ docker-compose up -d
```

#### Migrate container 

```sh
$ docker-compose exec app python manage.py migrate
```

#### Create Superuser

```sh
$ docker-compose exec app python manage.py createsuperuser
```

### Non-Docker Commands

#### Create the database docker container

```sh
$ docker run -p 5432:5432 --name spserver -e POSTGRES_PASSWORD=blank -e POSTGRES_USER=spserver -d postgres:alpine
```

#### Create a venv (python 3.6+)

```sh
$ python -m venv venv
```

or

```sh
$ python3 -m venv venv
```

#### Start venv

```sh
$ source venv/bin/active
```

#### Install dependencies

```sh
$ pip install -r requirements.txt
```

#### Applying migrations

```sh
$ python manage.py migrate
```

#### Run the Server

```sh
$ python manage.py runserver
```

## Test coverage

### Running tests

```sh
$ docker-compose exec app pytest
```

### or without docker

```sh
$ pytest
```