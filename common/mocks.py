from unittest import mock

from rest_framework import status

from common.factories import (
    GitHubAllUsersFactory,
    GitHubUserDetailsFactory,
    GitHubUserRepositoriesFactory,
)

MOCK_GITHUB_USERS_LIST = GitHubAllUsersFactory.build_batch(3)
MOCK_GITHUB_USER_REPOSITORIES = GitHubUserRepositoriesFactory.build_batch(3)

mock_responses = {
    'MOCK_GITHUB_USERS_LIST_LOCAL_RESPONSE': {
        'data': {
            'data': MOCK_GITHUB_USERS_LIST,
            'next_page': max(user['id'] for user in MOCK_GITHUB_USERS_LIST),
        },
        'status_code': status.HTTP_200_OK,
    },
    'MOCK_GITHUB_USERS_LIST_API_RESPONSE': mock.Mock(return_value={
        'data': MOCK_GITHUB_USERS_LIST,
        'status_code': status.HTTP_200_OK,
    }),
    'MOCK_GITHUB_USER_DETAILS': {
        'data': GitHubUserDetailsFactory(),
        'status_code': status.HTTP_200_OK,
    },
    'MOCK_GITHUB_USER_REPOSITORIES': {
        'data': MOCK_GITHUB_USER_REPOSITORIES,
        'status_code': status.HTTP_200_OK,
    },
}
