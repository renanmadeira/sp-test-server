from rest_framework import serializers


class GitHubAllUsersSerializer(serializers.Serializer):
    login = serializers.CharField(max_length=100)
    id = serializers.IntegerField()
    node_id = serializers.CharField(max_length=100)
    avatar_url = serializers.URLField(allow_blank=True, allow_null=True)
    gravatar_id = serializers.CharField(max_length=100, allow_blank=True)
    url = serializers.URLField(allow_blank=True, allow_null=True)
    html_url = serializers.URLField(allow_blank=True, allow_null=True)
    followers_url = serializers.URLField(allow_blank=True, allow_null=True)
    following_url = serializers.URLField(allow_blank=True, allow_null=True)
    gists_url = serializers.URLField(allow_blank=True, allow_null=True)
    starred_url = serializers.URLField(allow_blank=True, allow_null=True)
    subscriptions_url = serializers.URLField()
    organizations_url = serializers.URLField(allow_blank=True, allow_null=True)
    repos_url = serializers.URLField(allow_blank=True, allow_null=True)
    events_url = serializers.URLField(allow_blank=True, allow_null=True)
    received_events_url = serializers.URLField(allow_blank=True, allow_null=True)
    type = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    site_admin = serializers.BooleanField()


class GitHubUserDetailsSerializer(GitHubAllUsersSerializer):
    name = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    company = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    blog = serializers.URLField(allow_blank=True, allow_null=True)
    location = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    email = serializers.EmailField(allow_null=True)
    hireable = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    bio = serializers.CharField(max_length=255, allow_blank=True, allow_null=True)
    public_repos = serializers.IntegerField()
    public_gists = serializers.IntegerField()
    followers = serializers.IntegerField()
    following = serializers.IntegerField()
    created_at = serializers.DateTimeField(allow_null=True)
    updated_at = serializers.DateTimeField(allow_null=True)


class LicenseSerializer(serializers.Serializer):
    key = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    name = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    spdx_id = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    url = serializers.URLField(allow_blank=True, allow_null=True)
    node_id = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)


class GitHubUserRepositoriesSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    node_id = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    name = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    full_name = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    private = serializers.BooleanField()
    owner = GitHubAllUsersSerializer()
    html_url = serializers.URLField(allow_blank=True, allow_null=True)
    description = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    fork = serializers.BooleanField()
    url = serializers.URLField(allow_blank=True, allow_null=True)
    forks_url = serializers.URLField(allow_blank=True, allow_null=True)
    keys_url = serializers.URLField(allow_blank=True, allow_null=True)
    collaborators_url = serializers.URLField(allow_blank=True, allow_null=True)
    teams_url = serializers.URLField(allow_blank=True, allow_null=True)
    hooks_url = serializers.URLField(allow_blank=True, allow_null=True)
    issue_events_url = serializers.URLField(allow_blank=True, allow_null=True)
    events_url = serializers.URLField(allow_blank=True, allow_null=True)
    assignees_url = serializers.URLField(allow_blank=True, allow_null=True)
    branches_url = serializers.URLField(allow_blank=True, allow_null=True)
    tags_url = serializers.URLField(allow_blank=True, allow_null=True)
    blobs_url = serializers.URLField(allow_blank=True, allow_null=True)
    git_tags_url = serializers.URLField(allow_blank=True, allow_null=True)
    git_refs_url = serializers.URLField(allow_blank=True, allow_null=True)
    trees_url = serializers.URLField(allow_blank=True, allow_null=True)
    statuses_url = serializers.URLField(allow_blank=True, allow_null=True)
    languages_url = serializers.URLField(allow_blank=True, allow_null=True)
    stargazers_url = serializers.URLField(allow_blank=True, allow_null=True)
    contributors_url = serializers.URLField(allow_blank=True, allow_null=True)
    subscribers_url = serializers.URLField(allow_blank=True, allow_null=True)
    subscription_url = serializers.URLField(allow_blank=True, allow_null=True)
    commits_url = serializers.URLField(allow_blank=True, allow_null=True)
    git_commits_url = serializers.URLField(allow_blank=True, allow_null=True)
    comments_url = serializers.URLField(allow_blank=True, allow_null=True)
    issue_comment_url = serializers.URLField(allow_blank=True, allow_null=True)
    contents_url = serializers.URLField(allow_blank=True, allow_null=True)
    compare_url = serializers.URLField(allow_blank=True, allow_null=True)
    merges_url = serializers.URLField(allow_blank=True, allow_null=True)
    archive_url = serializers.URLField(allow_blank=True, allow_null=True)
    downloads_url = serializers.URLField(allow_blank=True, allow_null=True)
    issues_url = serializers.URLField(allow_blank=True, allow_null=True)
    pulls_url = serializers.URLField(allow_blank=True, allow_null=True)
    milestones_url = serializers.URLField(allow_blank=True, allow_null=True)
    notifications_url = serializers.URLField(allow_blank=True, allow_null=True)
    labels_url = serializers.URLField(allow_blank=True, allow_null=True)
    releases_url = serializers.URLField(allow_blank=True, allow_null=True)
    deployments_url = serializers.URLField(allow_blank=True, allow_null=True)
    created_at = serializers.DateTimeField(allow_null=True)
    updated_at = serializers.DateTimeField(allow_null=True)
    pushed_at = serializers.DateTimeField(allow_null=True)
    git_url = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    ssh_url = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    clone_url = serializers.URLField(allow_blank=True, allow_null=True)
    svn_url = serializers.URLField(allow_blank=True, allow_null=True)
    homepage = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    size = serializers.IntegerField()
    stargazers_count = serializers.IntegerField()
    watchers_count = serializers.IntegerField()
    language = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
    has_issues = serializers.BooleanField()
    has_projects = serializers.BooleanField()
    has_downloads = serializers.BooleanField()
    has_wiki = serializers.BooleanField()
    has_pages = serializers.BooleanField()
    forks_count = serializers.IntegerField()
    mirror_url = serializers.URLField(allow_blank=True, allow_null=True)
    archived = serializers.BooleanField()
    disabled = serializers.BooleanField()
    open_issues_count = serializers.IntegerField()
    license = LicenseSerializer(allow_null=True)
    forks = serializers.IntegerField()
    open_issues = serializers.IntegerField()
    watchers = serializers.IntegerField()
    default_branch = serializers.CharField(max_length=100, allow_blank=True, allow_null=True)
