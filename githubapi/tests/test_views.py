from unittest import mock

import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from common.factories import GitHubAllUsersFactory
from common.mocks import mock_responses


class GitHubAllUsersViewsetTest(APITestCase):
    def setUp(self):
        super().setUp()
        self.url = reverse('api:users_list')

    @pytest.mark.integration
    def test_retrieve_200_users_list(self):
        response = self.client.get(path=self.url, data={'since': 1})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.keys(), mock_responses['MOCK_GITHUB_USERS_LIST_LOCAL_RESPONSE']['data'].keys())
        self.assertEqual(response.data['data'][0].keys(), GitHubAllUsersFactory().keys())

    @mock.patch('requests.get', mock_responses['MOCK_GITHUB_USERS_LIST_API_RESPONSE'])
    def test_retrieve_400_users_list_missing_param(self):
        response = self.client.get(path=self.url)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @mock.patch('requests.get', mock_responses['MOCK_GITHUB_USERS_LIST_API_RESPONSE'])
    def test_retrieve_400_users_list_string_param(self):
        response = self.client.get(path=self.url, data={'since': 'test'})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class GitHubUserDetailsViewsetTest(APITestCase):
    def setUp(self):
        super().setUp()
        self.url = reverse('api:user_details', kwargs={'username': 'test'})

    @pytest.mark.integration
    def test_retrieve_200_user_details(self):
        response = self.client.get(path=self.url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.keys(), mock_responses['MOCK_GITHUB_USER_DETAILS']['data'].keys())

    @pytest.mark.integration
    def test_retrieve_404_user_details(self):
        response = self.client.get(path=reverse('api:user_details', kwargs={'username': 'test test test'}))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GitHubUserRepositoriesViewsetTest(APITestCase):
    def setUp(self):
        super().setUp()
        self.url = reverse('api:user_repositories', kwargs={'username': 'test'})

    @pytest.mark.integration
    def test_retrieve_200_user_repositories(self):
        response = self.client.get(path=self.url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0].keys(), mock_responses['MOCK_GITHUB_USER_REPOSITORIES']['data'][0].keys())

    @pytest.mark.integration
    def test_retrieve_404_user_repositories(self):
        response = self.client.get(path=reverse('api:user_repositories', kwargs={'username': 'test test test'}))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
