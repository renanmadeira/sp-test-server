from django.urls import path
from githubapi.viewsets import GitHubUserViewSet


urlpatterns = [
    path('users/', GitHubUserViewSet.as_view({"get": "users_list"}), name='users_list'),
    path('users/<str:username>/details', GitHubUserViewSet.as_view({"get": "user_details"}), name='user_details'),
    path('users/<str:username>/repos', GitHubUserViewSet.as_view({"get": "user_repositories"}), name='user_repositories'),
]
