import requests
from constance import config
from rest_framework import status, viewsets
from rest_framework.response import Response

from common.utils import isNum
from githubapi.serializers import (
    GitHubAllUsersSerializer,
    GitHubUserDetailsSerializer,
    GitHubUserRepositoriesSerializer,
)


class GitHubUserViewSet(viewsets.ViewSet):
    serializer_class = GitHubAllUsersSerializer
    details_serializer_class = GitHubUserDetailsSerializer
    repositories_serializer_class = GitHubUserRepositoriesSerializer

    def users_list(self, request):
        if not isNum(request.GET.get('since')):
            return Response({'error': 'Field since must be integer'}, status=status.HTTP_400_BAD_REQUEST)

        response = requests.get(url=config.GIT_HUB_USERS_END_POINT, params={'since': request.GET.get('since')})
        response.raise_for_status()

        serializer = self.serializer_class(data=response.json(), many=True)
        serializer.is_valid(raise_exception=True)

        max_id = max(dict(user)['id'] for user in serializer.data)

        return Response({
            'data': serializer.data,
            'next_page': max_id,
        }, status=status.HTTP_200_OK)

    def user_details(self, request, username):
        response = requests.get(url=config.GIT_HUB_USERS_END_POINT + f'/{username}')
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError:
            return Response({'error': 'Unable to find user'}, status=status.HTTP_404_NOT_FOUND)

        serializer = self.details_serializer_class(data=response.json())
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def user_repositories(self, request, username):
        response = requests.get(url=config.GIT_HUB_USERS_END_POINT + f'/{username}/repos')
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError:
            return Response({'error': 'Unable to find user'}, status=status.HTTP_404_NOT_FOUND)

        serializer = self.repositories_serializer_class(data=response.json(), many=True)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
